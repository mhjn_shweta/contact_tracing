upstream tornado {
    # Add multiple tornado server for load balancing
    server 127.0.0.1:8081;
    server 127.0.0.1:8082;
    server 127.0.0.1:8083;
    server 127.0.0.1:8084;
}

server {

    # listen 443 ssl;
    listen 8080;

    server_name apricot.com;

    access_log /var/log/nginx/safe-access.log;
    error_log /var/log/nginx/safe-error.log;

    # Comment out these lines if not running on ssl
    # ssl_certificate path/to/ssl_certificate.pem;
    # ssl_certificate_key path/to/ssl_certificate_key.key;

    set $root /code;

    location /static/ {
        expires 1y;
        add_header Pragma public;
        add_header Cache-Control "public";
        alias   $root/staticfiles/;
    }

    location /media/ {
        alias   $root/media/;
        expires 1y;
        add_header Pragma public;
        add_header Cache-Control "public";
    }

    location = /favicon.ico {
        return 204;
        access_log     off;
        log_not_found  off;
    }

    location / {
        include         uwsgi_params;
        uwsgi_pass      unix:/tmp/safe-uwsgi.sock;

        uwsgi_param Host $host;
        uwsgi_param X-Real-IP $remote_addr;
        uwsgi_param X-Forwarded-For $proxy_add_x_forwarded_for;
        uwsgi_param X-Forwarded-Proto $scheme;
    }

    location /ws/ {
        proxy_pass_header Server;
        proxy_set_header Host $http_host;
        proxy_redirect off;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Scheme $scheme;
        proxy_pass http://tornado;
    }

}
