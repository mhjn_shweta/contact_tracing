# Apricot server installation using Docker

## What is Docker?

Docker is an open-source project that automates the deployment of Linux applications inside software containers. You can setup a docker container with all the dependencies once and then run it on any machine with any operating system. Docker containers are kind of like Virtual Machines since they don't actually make any changes to your machine.

If you are interested in understanding how awesome docker is, read more at [https://prakhar.me/docker-curriculum/](https://prakhar.me/docker-curriculum/)

## Apricot server Installation
1. Install `Docker` on your machine, follow [this](https://docs.docker.com/engine/installation/) link for detailed installation instruction
2. Install `docker-compose`. Instruction [https://docs.docker.com/compose/install/](here). Easiest method is to use pip

    ```zsh
    pip install docker-compose # (use sudo if required)
    ```
3. Clone this repo and navigate to the root of the repository in the terminal
4. Build the docker containers using the following command (make sure you are in the directory which contains docker-compose.yml)

    ```zsh
    sudo docker-compose build
    ```
    Building the docker container for first time will take some time and if everything goes well you have successfully installed all the packages, dependencies and configuration required for Apricot to run!

  

## Running Apricot in Development Mode
1. Fire up docker containers in development mode using the command

    ```zsh
    sudo docker-compose -f docker-compose-dev.yml up
    ```

    This will start the containers, install all pip, npm and bower dependencies, and migrate all the db schema.

2. Wait till all the migrations are applied

    NOTE: To safely stop all the contianers navigate to the terminal where you invoked the `docker-compose up` command and press Ctrl-C.

3. Open a bash prompt inside the container and start django runserver and gulp watch
    ```zsh
    sudo docker-compose -f docker-compose-dev.yml exec django bash

    # Once inside the bash prompt in the container
    change to backend folder
    python manage.py runserver 0.0.0.0:8000
    ```

Now the dev version of the site is live at port 8000


**Remember `sudo docker-compose -f docker-compose-dev.yml exec django bash`. This lets you open a terminal inside the container and you can do anything as you would do if APRICOT and all it requirements are installed on your local system**


## Running Apricot in Production Mode

```zsh
sudo docker-compose up
```
Or if you want to run in detached mode use

```zsh
sudo docker-compose -d up
```

To safely stop all the contianers

```zsh
# If you're using detached mode
sudo docker-compose down

# Else press Ctrl-C to stop the containers
```
