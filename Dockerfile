FROM datagovsg/python-node:3.5-6.9.4

# Set ENV for HTTP Proxy
ENV http_proxy ${http_proxy}
ENV https_proxy ${https_proxy}

# Install required packages through apt-get
RUN echo "deb [check-valid-until=no] http://archive.debian.org/debian jessie-backports main" > /etc/apt/sources.list.d/jessie-backports.list
RUN sed -i '/deb http:\/\/deb.debian.org\/debian jessie-updates main/d' /etc/apt/sources.list
RUN apt-get -o Acquire::Check-Valid-Until=false update

RUN apt-get install -y \
    build-essential \
    git \
	libjpeg62-turbo-dev libsasl2-dev libldap2-dev libssl-dev \
	psmisc \
	tcl8.5 \
	p7zip-full \
	cmake

RUN apt-get install --no-install-recommends -y libreoffice
RUN apt-get install -y gettext

# Create a non root user
# Set password for root user for debugging purposes
RUN useradd --create-home --shell /bin/bash apricot && \
    echo 'root:root123' | chpasswd

ADD . /home/apricot/code
WORKDIR /home/apricot/code

# Install virtualenv
# Install npm global dependencies
# Copy uwsgi.conf and super.cfg to root path
RUN pip3 install -r backend/requirements/local.txt -r backend/requirements/production.txt && \
	npm install -g bower gulp yarn

USER apricot
ENV PYTHONUNBUFFERED 1
