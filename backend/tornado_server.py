#! /usr/bin/env python
"""
Python file to start tornado server for chat application.
It reads TORNADO_PORT from django settings and start a tornado ioloop instance on that port.
"""
import logging
import os
import signal
import sys
import time

import django
import tornado
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.wsgi
from tornado.options import define, options, parse_command_line

MAX_WAIT_SECONDS_BEFORE_SHUTDOWN = 3
server = None


define('addr', help='Address to start server', default='127.0.0.1')
define('port', help='Port to start tornado', type=int, default=8081)
define('settings', help='Django settings module', type=str)


def sig_handler(sig, frame):  # pylint: disable=W0613
    """
    capture and handle single by calling shutdown

    Args:
        sig: signal number arrived
        frame: Frame object
    """
    tornado.ioloop.IOLoop.current().add_callback(shutdown)


def get_tornado_app():
    """
    Return a tornado web application instance which includes url config
    """

    return tornado.web.Application(

    )


def shutdown():
    """
    Gracefully shutdown the tornado server.

    It will wait for MAX_WAIT_SECONDS_BEFORE_SHUTDOWN to finish any callback and timeouts before
    shutting down completely.
    """
    logging.info('Stopping http server')
    server.stop()

    logging.info('Will shutdown in %s seconds ...', MAX_WAIT_SECONDS_BEFORE_SHUTDOWN)
    io_loop = tornado.ioloop.IOLoop.current()

    deadline = time.time() + MAX_WAIT_SECONDS_BEFORE_SHUTDOWN

    def stop_loop():
        """
        Gracefully wait for io_loop to finish callbacks and timeouts before shutting it.
        """
        now = time.time()
        if now < deadline and (io_loop._callbacks or io_loop._timeouts):  # pylint: disable=W0212
            io_loop.add_timeout(now + 1, stop_loop)
        else:
            io_loop.stop()
            logging.info('Shutdown')
    stop_loop()


def main():
    """
    Configure and start tornado server
    """
    parse_command_line()

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'apricot.settings.local')
    if options.settings:
        os.environ['DJANGO_SETTINGS_MODULE'] = options.settings

    django.setup()

    from django.utils import timezone

    global server  # pylint: disable=W0603

    tornado_app = get_tornado_app()
    server = tornado.httpserver.HTTPServer(tornado_app)
    server.listen(options.port, options.addr)

    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGINT, sig_handler)

    quit_command = 'CTRL-BREAK' if sys.platform == 'win32' else 'CONTROL-C'
    logging.info(timezone.now().strftime('%B %d, %Y - %X'))
    # pylint: disable=logging-format-interpolation
    logging.info(
        'Tornado Version: {tornado_version}\n'
        'Django Version: {django_version}, using settings \'{settings}\'\n'
        'Starting tornado server at {addr}:{port}\n'
        'Quit Server with {quit_command}'.format(
            tornado_version=tornado.version,
            django_version=django.get_version(),
            settings=options.settings,
            addr=options.addr,
            port=options.port,
            quit_command=quit_command,
        )
    )
    # pylint: enable=logging-format-interpolation

    tornado.ioloop.IOLoop.current().start()


if __name__ == '__main__':
    main()
