-r common.txt
django-debug-toolbar==1.7
prospector==0.12.7
pep8==1.7.0
isort==4.2.5
Werkzeug==0.11.10
django-extensions==1.6.7
django-debug-panel==0.8.3
django-cors-headers==1.1.0
pre-commit==1.8.2