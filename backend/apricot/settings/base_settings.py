

import os
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import sys


from .conf import *  # pylint: disable=W0614,W0401

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


# Application definition



INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'compressor',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_framework_swagger',
    'parsley',


]

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'simple_history.middleware.HistoryRequestMiddleware',

]

ROOT_URLCONF = 'apricot.urls'

LOGIN_URL = '/account/login/'
LOGIN_REDIRECT_URL = '/'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'apricot.context_processor.apricot_global_context',
            ],
        },
    },
]

WSGI_APPLICATION = 'apricot.wsgi.application'


# Password validation


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en-us'

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'locale'),
]

TIME_ZONE = 'Asia/Kolkata'

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')

STATICFILES_DIRS = (
    # Add all static files here. use os.path.join(BASE_DIR, 'your/staticfiles/path')
    os.path.join(BASE_DIR, 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)


REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),
}

FIXTURE_DIRS = [
    os.path.join(BASE_DIR, 'apricot', 'fixtures')
]


# Cache settings
PUBSUB_HOST = REDIS_HOST_NAME or 'localhost'
PUBSUB_PORT = REDIS_PORT or 6379
PUBSUB_DB = 4

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': [
            'redis://{host}:{port}/0'.format(host=PUBSUB_HOST, port=PUBSUB_PORT),

        ],
    },
    'pubsub': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': [
            'redis://{host}:{port}/{db}'.format(host=PUBSUB_HOST, port=PUBSUB_PORT, db=PUBSUB_DB),
        ]
    },
}

# Using cache as main session engine
SESSION_ENGINE = 'django.contrib.sessions.backends.cache'


# Celery settings
BROKER_URL = 'redis://{host}:{port}/2'.format(host=REDIS_HOST_NAME, port=REDIS_PORT)
CELERY_RESULT_BACKEND = BROKER_URL
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_IGNORE_RESULT = True
CELERY_MAX_CACHED_RESULTS = None  # Infinite


# Testing specific configuration
if 'test' in sys.argv:
    CACHES['default']['LOCATION'] \
        = ['redis://{host}:{port}/1'.format(host=REDIS_HOST_NAME,
                                            port=REDIS_PORT)]

# Swagger Settings
SWAGGER_SETTINGS = {
    'info': {
        'title': 'A Privacy preserving contact tracing system',
        'description': 'An application to trace contacts',
        'contact': 'br@cse.iitb.ac.in',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_USE_TLS = False

EMAIL_USE_SSL = True

EMAIL_HOST = ''

EMAIL_HOST_USER = ''

#Must generate specific password for your app in [gmail settings][1]
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = 465

DEFAULT_FROM_EMAIL = EMAIL_HOST_USER