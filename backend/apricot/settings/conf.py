"""
Configuration file for settings
"""

SECRET_KEY = '<SECRET KEY>'  # noqa

ALLOWED_HOSTS = ['*']

DB_NAME = 'apricot'

DB_USERNAME = 'apricot'

DB_PASSWORD = 'apricot'  # noqa

# DB_HOST_NAME = 'localhost'

# When using Docker
DB_HOST_NAME = 'db'

DB_PORT = '5432'

ADMINS_EMAIL_LIST = [
    # ('Name', 'email@example.com'),
]

# REDIS_HOST_NAME = 'localhost'

# When using Docker
REDIS_HOST_NAME = 'redis'

REDIS_PORT = '6379'
