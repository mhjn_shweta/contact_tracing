"""
DJANGO_SETTINGS_MODULE for production
"""

import logging.config

from .base_settings import *  # pylint: disable=W0614,W0401
from .conf import *  # pylint: disable=W0401

DEBUG = False

ALLOWED_HOSTS = ['*']

STATIC_URL = '/static/'

MEDIA_URL = '/media/'

SAMPLE_FILE_URL = '/sample_files/'

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.CachedStaticFilesStorage'

TEMPLATES[0]['DIRS'] = [os.path.join(BASE_DIR, 'templates/gen')]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': DB_NAME,
        'USER': DB_USERNAME,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST_NAME,
        'PORT': DB_PORT,
        'ATOMIC_REQUESTS': False,
    }
}

# Swagger setting

SWAGGER_SETTINGS['is_superuser'] = True

# Logging Configuration
LOGGING_CONFIG = None
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s [%(asctime)s] [%(name)s] [%(module)s] [Process:%(process)d] '
                      '[Thread:%(thread)d] %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        }
    },
    'handlers': {

    },
    'loggers': {

    },

}
logging.config.dictConfig(LOGGING)

X_FRAME_OPTIONS = 'DENY'
