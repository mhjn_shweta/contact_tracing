"""
DJANGO_SETTINGS_MODULE for local development
"""

from .base_settings import *  # pylint: disable=W0614,W0401
from .conf import *  # pylint: disable=W0401

SECRET_KEY = 'tbz2ahf(!%t=1_k#j!xn%^rnza9v@$@)e#63)hn42gv4epf6fa'

DEBUG = True

INSTALLED_APPS = [
    # 'django_gulp',
] + INSTALLED_APPS

INSTALLED_APPS += [
    'debug_toolbar',
    'django_extensions',
    'debug_panel',
    'corsheaders',
    # 'apps.request_logging',
]

MIDDLEWARE_CLASSES = [
#     'apps.request_logging.middleware.RequestLoggingMiddleware',
    'corsheaders.middleware.CorsMiddleware',
] + MIDDLEWARE_CLASSES

MIDDLEWARE_CLASSES = [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'debug_panel.middleware.DebugPanelMiddleware',
] + MIDDLEWARE_CLASSES

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': DB_NAME,
        'USER': DB_USERNAME,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST_NAME,
        'PORT': DB_PORT,
        'ATOMIC_REQUESTS': False,
    }
}

STATIC_URL = '/static/'

MEDIA_URL = '/media/'

SAMPLE_FILE_URL = '/sample_files/'

# CORS Setup

CORS_ORIGIN_WHITELIST = (
    'localhost:5555',
)


# Show debug toolbar without
def show_toolbar(request):  # pylint: disable=unused-argument
    return True


DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK" : show_toolbar,
}
