"""
Custom context processors for templates to keep global key values
"""

from django.conf import settings
from django.http import HttpRequest


def apricot_global_context(request: HttpRequest) -> dict:  # pylint: disable=unused-argument
    """
    Global context processor for Apricot templates

    Args:
        request: Django HttpRequest object. Used when context are request dependent
    """

    return {
        'IS_DEBUG': settings.DEBUG,
    }
