#! /bin/bash

. super.cfg

export DJANGO_SETTINGS_MODULE=${DJANGO_SETTINGS_MODULE}
SCRIPT_PATH=`pwd`

PULL_CODE=true;
DO_MIGRATE=true;
DO_COLLECTSTATIC=true;
DO_COMPILE=true;
DO_PIP_INSTALL=true;
DO_NPM_INSTALL=true;
DEVELOP_MODE=false;


function pull_code(){
    if [ "$PULL_CODE" = true ]; then
        echo -e "\nPulling latest code";
        git pull;
    fi;
}

function django(){
    if [ "$DO_MIGRATE" = true ]; then
        echo -e "\nRunning migrations";
        python backend/manage.py migrate;
    fi;

    if [ "$DO_COLLECTSTATIC" = true ]; then
        echo -e "\nCollecting static files";
        python backend/manage.py collectstatic --no-input;
    fi;
}

function compile(){
    if [ "$DO_COMPILE" = true ]; then
        echo -e "\nRunning Frontend Build";
        gulp production;
    fi;
}

function pip_install(){
    if [ "$DO_PIP_INSTALL" = true ]; then
        echo -e "\nInstalling Dependencies";
        pip install -r backend/requirements/production.txt;

        if [ "$DEVELOP_MODE" = true ]; then
            pip install -r backend/requirements/local.txt;
        fi;
    fi;
}


function npm_install(){
    if [ "$DO_NPM_INSTALL" = true ]; then
        echo -e "\nInstalling NPM Packages";
        yarn install;
    fi;
}

#########################################
## uWSGI functions

function start_uwsgi(){
    echo -e "\nStarting uWSGI";
    uwsgi --ini ${UWSGI_INI_FILE};
}

function stop_uwsgi(){
    echo -e "\nStopping uWSGI";
    uwsgi --stop ${UWSGI_PID_FILE};
}

function reload_uwsgi(){
    echo -e "\nReloading uWSGI";
    uwsgi --reload ${UWSGI_PID_FILE};
    if [ $? -ne 0 ]; then
        start_uwsgi;
    fi;
}
##########################################


##############################################
## Celery functions
CELERY_MULTI="celery multi"
CELERY_OPTIONS="${CELERY_WORKERS} -c ${CELERY_PROCESSES_PER_WORKER} \
-A apricot -l info \
--logfile=${CELERY_LOGFILE} \
--pidfile=${CELERY_PIDFILE} \
--workdir=${SCRIPT_PATH}/backend/"

function start_celery(){
    echo -e "\nStarting Celery";
    local command="$CELERY_MULTI start $CELERY_OPTIONS"
    eval $command;
}

function stop_celery(){
    echo -e "\nStopping Celery";
    local command="$CELERY_MULTI stop $CELERY_OPTIONS";
    eval $command;
}

function reload_celery(){
    echo -e "\nReloading Celery";
    local command="$CELERY_MULTI restart $CELERY_OPTIONS";
    eval $command;
}

##############################################


###########################################################
## Tornado functions
tornado_ports=$(echo ${TORNDO_PORTS} | tr "," "\n");

function start_tornado(){
    echo -e "\nStarting Tornado server";
    for port in ${tornado_ports}
    do
        echo "Starting tornado at port ${port}"
        python backend/tornado_server.py --addr=0.0.0.0 --port=${port} &
    done
}

function stop_tornado(){
    echo -e "\nStopping tornado server";
    for port in ${tornado_ports}
    do
        echo "Stopping tornado at port ${port}"
        fuser -k ${port}/tcp;
    done
}

function reload_tornado(){
    echo -e "\nReloading tornado server";
    for port in ${tornado_ports}
    do
        echo -e "\nGracefully shutting down tornado at port ${port}"
        fuser -k -SIGINT ${port}/tcp;

        echo "Starting tornado at port ${port}"
        python backend/tornado_server.py --addr=0.0.0.0 --port=${port} &
    done
}

###########################################################333

function start(){


    if [ "$DEVELOP_MODE" = false ]; then
        start_uwsgi;
        start_tornado;
    fi
}

function stop(){


    if [ "$DEVELOP_MODE" = false ]; then
        stop_uwsgi;
        stop_tornado;
    fi
}

function reload(){


    if [ "$DEVELOP_MODE" = false ]; then
        reload_uwsgi;
        reload_tornado;
    fi
}

function usage(){
    echo "
Usage: $0 start|stop|reload [--dont-pull|-dp] [--dont-migrate|-dm] [--dont-collectstatic|-dc] [--develop-mode|-dev]

Positional Argument:
start                               Start uwsgi, celery and tornado
stop                                Stop uwsgi, celery and tornado
reload                              Gracefully reload uwsgi, celery and tornado

--
Optional Arguments:
Presence of these options means it is supplied as true. Don not use them with '=' sign

--dont-pull|-dp                     Do not pull code from git repo. Default: false
--dont-migrate|-dm                  Do not Django migrate command. Default: false
--dont-collectstatic|-dc            Do not run Django collectstatic command. Default false
--dont-compile|-dcom                Do not compile typescripts
--develop-mode|-dev                 Run in development mode, do not start tornado and uwsgi

These all are false when server is stopped.
"
}
# Function to check if value is in array
function contains() {
    local n=$#
    local value=${!n}
    for ((i=1;i < $#;i++)) {
        if [ "${!i}" == "${value}" ]; then
            echo "y"
            return 0
        fi
    }
    echo "n"
    return 1
}


valid_commands=( "start" "stop" "reload" );

if [ $(contains "${valid_commands[@]}" "$1") == "n" ]; then
    echo -e "Invalid positional argument: $1" 1>&2;
    usage;
    exit 1;
fi


case "$1" in
    stop)
    PULL_CODE=false;
    DO_MIGRATE=false;
    DO_COLLECTSTATIC=false;
    DO_COMPILE=false;
    DO_PIP_INSTALL=false;
	DO_NPM_INSTALL=false;
    DEVELOP_MODE=false;
    ;;
    *)
    ;;
esac


for i in "$@"
do
case ${i} in
    --dont-pull|-dp)
    PULL_CODE=false;
    ;;
    --dont-migrate|-dm)
    DO_MIGRATE=false;
    ;;
    --dont-collectstatic|-dc)
    DO_COLLECTSTATIC=false;
    ;;
    --dont-compile|-dcom)
    DO_COMPILE=false;
    ;;
    --no-pip-install|-npi)
    DO_PIP_INSTALL=false;
    ;;
    --no-npm-install|-nnpm)
    DO_NPM_INSTALL=false;
    ;;
    --develop-mode|-dev)
    DEVELOP_MODE=true;
    ;;
    --help)
    usage;
    exit;
    ;;
    *)

    ;;
esac
done

pull_code;
pip_install;
npm_install;
django;


case "$1" in
    start)
    start;
    ;;
    stop)
    stop;
    ;;
    reload)
    reload;
esac
